class JsonRpcRequest {
  static const String jsonrpc = '2.0';
  final String? id;
  final String method;
  final Object? params;

  JsonRpcRequest({this.id, required this.method, this.params});

  bool isValid() {
    return method.isNotEmpty;
  }

  bool isNotification() {
    return id == null;
  }

  Map<String, dynamic> toJson() {
    if (id == null) {
      // notification
      final Map<String, dynamic> result = {'jsonrpc': jsonrpc, 'method': method};
      if (params != null) {
        result['params'] = params;
      }
      return result;
    }

    final Map<String, dynamic> result = {'jsonrpc': jsonrpc, 'id': id, 'method': method};
    if (params != null) {
      result['params'] = params;
    }
    return result;
  }

  factory JsonRpcRequest.fromJson(Map<String, dynamic> json) {
    final String? id = json['id'];
    final String method = json['method'];
    final Object? params = json['params'];
    return JsonRpcRequest(id: id, method: method, params: params);
  }
}

class Error {
  final int? code;
  final String? message;

  Error(this.code, this.message);

  Map<String, dynamic> toJson() {
    return {'code': code, 'message': message};
  }

  Error.fromJson(Map<String, dynamic> json)
      : code = json['code'],
        message = json['message'];
}

class JsonRpcResponse {
  static const String jsonrpc = '2.0';
  final String id;
  final Object? result;
  final Error? error;

  JsonRpcResponse({required this.id, this.result, this.error});

  bool isValid() {
    return id.isNotEmpty;
  }

  bool isMessage() {
    return result != null;
  }

  bool isError() {
    return error != null;
  }

  Map<String, dynamic> toJson() {
    if (isError()) {
      return {'jsonrpc': jsonrpc, 'id': id, 'error': error};
    }

    return {'jsonrpc': jsonrpc, 'id': id, 'result': result};
  }

  factory JsonRpcResponse.fromJson(Map<String, dynamic> json) {
    if (!json.containsKey('id')) {
      throw 'Invalid input, id required';
    }

    final id = json['id']; // must be according RPC protocol
    Error? error;
    if (json.containsKey('error')) {
      error = Error.fromJson(json['error']);
    }
    final Object? result = json['result'];

    // must be error or result
    if (error == null && result == null) {
      throw 'Invalid input, must be error or result field';
    }

    if (error != null && result != null) {
      throw 'Invalid input, must be error or result field not both';
    }

    return JsonRpcResponse(id: id, result: result, error: error);
  }
}
