class NotificationType {
  final int _value;

  const NotificationType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == 0) {
      return 'Text';
    }
    return 'Hyperlink';
  }

  factory NotificationType.fromInt(int? value) {
    if (value == 0) {
      return NotificationType.TEXT;
    }
    return NotificationType.HYPERLINK;
  }

  static List<NotificationType> get values => [TEXT, HYPERLINK];

  static const NotificationType TEXT = NotificationType._(0);
  static const NotificationType HYPERLINK = NotificationType._(1);
}

class NotificationTextInfo {
  static const TEXT_FIELD = 'message';
  static const TYPE_FIELD = 'type';
  static const SHOW_TIME_FIELD = 'show_time';

  String message;
  NotificationType type;
  int showTime;

  static const MIN_TIME_MSEC = 1 * 1000;
  static const MAX_TIME_MSEC = 3600 * 1000;

  NotificationTextInfo(this.message, this.type, this.showTime);

  bool isValid() {
    return message.isNotEmpty && showTime >= MIN_TIME_MSEC && showTime <= MAX_TIME_MSEC;
  }

  factory NotificationTextInfo.fromJson(Map<String, dynamic> json) {
    final message = json[TEXT_FIELD];
    final type = NotificationType.fromInt(json[TYPE_FIELD]);
    final showTime = json[SHOW_TIME_FIELD];
    return NotificationTextInfo(message, type, showTime);
  }

  Map<String, dynamic> toJson() {
    return {TEXT_FIELD: message, TYPE_FIELD: type.toInt(), SHOW_TIME_FIELD: showTime};
  }
}
