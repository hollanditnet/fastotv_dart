String _twoDigits(int n) {
  if (n >= 10) {
    return "$n";
  }
  return "0$n";
}

class ProgrammeInfo {
  static const CHANNEL_FIELD = 'channel';
  static const START_FIELD = 'start';
  static const STOP_FIELD = 'stop';
  static const TITLE_FIELD = 'title';
  static const CATEGORY_FIELD = 'category';
  static const DESCRIPTION_FIELD = 'description';

  final String channel;
  final int start;
  final int stop;
  final String title;
  final String? category;
  final String? description;

  ProgrammeInfo(this.channel, this.start, this.stop, this.title, this.category, this.description);

  factory ProgrammeInfo.fromJson(Map<String, dynamic> json) {
    final channel = json[CHANNEL_FIELD];
    final start = json[START_FIELD];
    final stop = json[STOP_FIELD];
    final title = json[TITLE_FIELD];
    final category = json[CATEGORY_FIELD];
    final desc = json[DESCRIPTION_FIELD];
    return ProgrammeInfo(channel, start, stop, title, category, desc);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {
      CHANNEL_FIELD: channel,
      START_FIELD: start,
      STOP_FIELD: stop,
      TITLE_FIELD: title
    };
    if (category != null) {
      result[CATEGORY_FIELD] = category;
    }
    if (description != null) {
      result[DESCRIPTION_FIELD] = description;
    }
    return result;
  }

  String getStart(Duration timeZoneOffset) {
    final startTime = Duration(milliseconds: start) + timeZoneOffset;
    final diff = startTime - Duration(days: startTime.inDays);

    final hours = diff.inHours;
    final minutes = (diff - Duration(hours: diff.inHours)).inMinutes;
    return "${_twoDigits(hours)}:${_twoDigits(minutes)}";
  }

  String getEnd(Duration timeZoneOffset) {
    final Duration stopTime = Duration(milliseconds: stop) + timeZoneOffset;
    final Duration diff = stopTime - Duration(days: stopTime.inDays);

    final int hours = diff.inHours;
    final int minutes = (diff - Duration(hours: diff.inHours)).inMinutes;
    return "${_twoDigits(hours)}:${_twoDigits(minutes)}";
  }

  String getDuration() {
    final Duration startTime = Duration(milliseconds: start);
    final Duration stopTime = Duration(milliseconds: stop);
    final Duration diff = stopTime - startTime;
    final String twoDigitMinutes = _twoDigits(diff.inMinutes.remainder(60));
    return '${_twoDigits(diff.inHours)}:$twoDigitMinutes';
  }
}
