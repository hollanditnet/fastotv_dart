import 'package:fastotv_dart/commands_info/epg_info.dart';
import 'package:fastotv_dart/commands_info/meta_url.dart';
import 'package:fastotv_dart/commands_info/stream_base_info.dart';

class ChannelInfo extends StreamBaseInfo {
  static const EPG_FIELD = 'epg';
  static const ARCHIVE_FIELD = 'archive';

  final EpgInfo epg;
  final bool archive;

  ChannelInfo(
      String id,
      List<String> groups,
      int iarc,
      bool favorite,
      int recent,
      int interrupt_time,
      bool locked,
      this.epg,
      bool video,
      bool audio,
      List<String> parts,
      int view_count,
      List<MetaUrl> meta,
      this.archive)
      : super(id, groups, iarc, favorite, recent, interrupt_time, locked, video, audio, parts,
            view_count, meta);

  String displayName() {
    return epg.display_name;
  }

  String primaryLink() {
    return epg.urls[0];
  }

  String icon() {
    return epg.icon;
  }

  factory ChannelInfo.fromJson(Map<String, dynamic> json) {
    final base = StreamBaseInfo.fromJson(json);
    final epg = EpgInfo.fromJson(json[EPG_FIELD]);
    final archive = json[ARCHIVE_FIELD];

    return ChannelInfo(
        base.id,
        base.groups,
        base.iarc,
        base.favorite,
        base.recent,
        base.interrupt_time,
        base.locked,
        epg,
        base.video,
        base.audio,
        base.parts,
        base.view_count,
        base.meta,
        archive);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> base = super.toJson();
    base[EPG_FIELD] = epg.toJson();
    return base;
  }
}
