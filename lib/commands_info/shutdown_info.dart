class ShutdownInfo {
  static const TIMEOUT_FIELD = 'timeout';

  static const MIN_TIME_MSEC = 0;
  static const MAX_TIME_MSEC = 3600 * 1000;

  int timeout;

  ShutdownInfo(this.timeout);

  bool isValid() {
    return timeout >= MIN_TIME_MSEC && timeout <= MAX_TIME_MSEC;
  }

  factory ShutdownInfo.fromJson(Map<String, dynamic> json) {
    final time = json[TIMEOUT_FIELD] ?? 0;
    return ShutdownInfo(time);
  }

  Map<String, dynamic> toJson() {
    return {TIMEOUT_FIELD: timeout};
  }
}
