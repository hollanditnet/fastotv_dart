class RuntimeChannelLiteInfo {
  static const ID_FIELD = 'id';
  final String id;

  RuntimeChannelLiteInfo(this.id);

  Map<String, dynamic> toJson() => {ID_FIELD: id};
}

class RuntimeChannelInfo extends RuntimeChannelLiteInfo {
  static const WATCHERS_FIELD = 'watchers';
  final int watchers;

  RuntimeChannelInfo(id, this.watchers) : super(id);

  @override
  Map<String, dynamic> toJson() {
    return {RuntimeChannelLiteInfo.ID_FIELD: id, WATCHERS_FIELD: watchers};
  }
}
