class RecentInfo {
  static const ID_FIELD = 'id';
  static const TIME_FIELD = 'time';

  final String id;
  final int time;

  RecentInfo(this.id, this.time);

  Map<String, dynamic> toJson() {
    return {ID_FIELD: id, TIME_FIELD: time};
  }
}
