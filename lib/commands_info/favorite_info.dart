class FavoriteInfo {
  static const ID_FIELD = 'id';
  static const FAVORITE_FIELD = 'favorite';

  final String id;
  final bool favorite;

  FavoriteInfo(this.id, this.favorite);

  Map<String, dynamic> toJson() {
    return {ID_FIELD: id, FAVORITE_FIELD: favorite};
  }
}
