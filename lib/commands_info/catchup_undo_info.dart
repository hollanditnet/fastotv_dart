class CatchupUndoInfo {
  static const ID_FIELD = 'id';

  final String id;

  CatchupUndoInfo(this.id);

  Map<String, dynamic> toJson() {
    return {ID_FIELD: id};
  }
}
