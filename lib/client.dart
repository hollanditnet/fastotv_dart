import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart';
import 'package:fastotv_dart/commands.dart';
import 'package:fastotv_dart/commands_info/auth_info.dart';
import 'package:fastotv_dart/commands_info/catchup_generate_info.dart';
import 'package:fastotv_dart/commands_info/catchup_undo_info.dart';
import 'package:fastotv_dart/commands_info/client_info.dart';
import 'package:fastotv_dart/commands_info/content_request_info.dart';
import 'package:fastotv_dart/commands_info/favorite_info.dart';
import 'package:fastotv_dart/commands_info/interrupt_stream_info.dart';
import 'package:fastotv_dart/commands_info/login_info.dart';
import 'package:fastotv_dart/commands_info/recent_info.dart';
import 'package:fastotv_dart/commands_info/runtime_channel_info.dart';
import 'package:fastotv_dart/json_rpc.dart';
import 'package:fastotv_dart/src/commands_json.dart';

String generateHash(String data) {
  return md5.convert(utf8.encode(data)).toString();
}

enum ClientConnectionState { DISCONNECTED, CONNECTED, ACTIVE }
enum CompressedType { C_NONE, C_STANDART }

abstract class IClientObserver {
  void processRequest(JsonRpcRequest req);

  void processResponse(JsonRpcRequest req, JsonRpcResponse resp);

  void onConnectionStateChanged(ClientConnectionState newState);

  void onSocketError(Object error);

  void onDisconnected(Object exception);
}

class Client {
  Socket? _socket;
  IClientObserver? _observer;
  int _id = 0;
  final Map<String?, JsonRpcRequest> _requests = {};
  List<int> _recvBuffer = [];
  final CompressedType _compressed = CompressedType.C_STANDART;

  Client();

  void setObserver(IClientObserver observer) {
    _observer = observer;
  }

  String generateID() {
    final int id = _id++;
    final Uint8List message = Uint8List(8);
    final ByteData bytedata = ByteData.view(message.buffer);
    bytedata.setUint64(0, id);
    return hex.encode(message);
  }

  void connect(String server, int port) async {
    if (_socket != null) {
      _socket!.destroy();
      _socket = null;
    }

    try {
      _socket = await Socket.connect(server, port);
      _socket!.listen(_socketDataReceived,
          onError: _socketErrorReceived, onDone: _socketDone, cancelOnError: false);
      _observer?.onConnectionStateChanged(ClientConnectionState.CONNECTED);
    } catch (exception) {
      _observer?.onConnectionStateChanged(ClientConnectionState.DISCONNECTED);
      _observer?.onDisconnected(exception);
    }
  }

  void activate(String email, String password) {
    if (email.isEmpty || password.isEmpty) {
      return;
    }

    final String hash = generateHash(password);
    final LoginInfo user = LoginInfo(email, hash);
    final JsonRpcRequest request = activateRequest(generateID(), user);
    return _sendRequest(request);
  }

  void login(String email, String password, String deviceId) {
    if (email.isEmpty || password.isEmpty || deviceId.isEmpty) {
      return;
    }

    final String hash = generateHash(password);
    final AuthInfo user = AuthInfo(email, hash, deviceId);
    final JsonRpcRequest request = loginRequest(generateID(), user);
    return _sendRequest(request);
  }

  void requestCatchup(String sid, String title, int start, int stop) {
    final CatchupGenerateInfo cat = CatchupGenerateInfo(sid, title, start, stop);
    final JsonRpcRequest request = catchupRequest(generateID(), cat);
    return _sendRequest(request);
  }

  void requestUndoCatchup(String sid) {
    final CatchupUndoInfo cat = CatchupUndoInfo(sid);
    final JsonRpcRequest request = catchupUndoRequest(generateID(), cat);
    return _sendRequest(request);
  }

  void requestChannels() {
    final JsonRpcRequest request = getChannelsRequest(generateID());
    return _sendRequest(request);
  }

  void requestRuntimeChannelInfo(String sid) {
    final RuntimeChannelLiteInfo run = RuntimeChannelLiteInfo(sid);
    final JsonRpcRequest request = getRuntimeChannelInfoRequest(generateID(), run);
    return _sendRequest(request);
  }

  void requestContent(CreateContentRequestInfo info) {
    final JsonRpcRequest request = createContentRequest(generateID(), info);
    return _sendRequest(request);
  }

  void sendFavoriteInfo(String sid, bool value) {
    final FavoriteInfo fav = FavoriteInfo(sid, value);
    final JsonRpcRequest request = setFavoriteRequest(sid, fav);
    return _sendRequest(request);
  }

  void sendRecentInfo(String sid, int value) {
    final RecentInfo recent = RecentInfo(sid, value);
    final JsonRpcRequest request = addRecentRequest(sid, recent);
    return _sendRequest(request);
  }

  void sendInterruptInfo(String sid, int msec) {
    final InterruptStreamTimeInfo inter = InterruptStreamTimeInfo(sid, msec);
    final JsonRpcRequest request = interruptStreamTimeRequest(sid, inter);
    return _sendRequest(request);
  }

  void ping() {
    final now = DateTime.now();
    final utc = now.toUtc();
    final JsonRpcRequest request =
        pingRequest(generateID(), {'timestamp': utc.millisecondsSinceEpoch});
    return _sendRequest(request);
  }

  void getServerInfo() {
    final JsonRpcRequest request = getServerInfoRequest(generateID());
    return _sendRequest(request);
  }

  void pong(String id) {
    final now = DateTime.now();
    final utc = now.toUtc();
    final JsonRpcResponse resp = pongResponse(id, {'timestamp': utc.millisecondsSinceEpoch});
    return _sendResponse(resp);
  }

  void notificationTextOK(String id) {
    final JsonRpcResponse resp = pongResponse(id, {});
    return _sendResponse(resp);
  }

  void clientInfo(String id, ClientInfo cl) {
    final JsonRpcResponse resp = clientInfoResponse(id, cl);
    return _sendResponse(resp);
  }

  void dispose() {
    _socket?.destroy();
    _socket = null;
  }

  // private:
  void _sendRequest(JsonRpcRequest request) {
    if (_socket == null) {
      return;
    }

    if (!request.isNotification()) {
      _requests[request.id] = request;
    }
    final String req = json.encode(request);
    final List<int> toSend = _prepareToSend(req);
    _send(toSend);
  }

  void _sendResponse(JsonRpcResponse resp) {
    if (_socket == null) {
      return;
    }

    final String respStr = json.encode(resp);
    final List<int> toSend = _prepareToSend(respStr);
    _send(toSend);
  }

  void _send(List<int> data) {
    final Uint8List message = Uint8List(4);
    final ByteData byteData = ByteData.view(message.buffer);
    byteData.setUint32(0, data.length);

    _socket!.add(message);
    _socket!.add(data);
  }

  void _socketDone() {
    _observer?.onConnectionStateChanged(ClientConnectionState.DISCONNECTED);
    _socket!.destroy();
    _socket = null;
  }

  void _socketErrorReceived(error, trace) {
    _observer?.onSocketError(error);
  }

  bool _processMessage(List<int> payload) {
    final List<int> decoded = _prepareToReceive(payload);
    final String reqOrRespData = String.fromCharCodes(decoded);
    final Map<String, dynamic> requestOrResponse = json.decode(reqOrRespData);
    if (requestOrResponse.isEmpty) {
      return false;
    }

    if (requestOrResponse.containsKey('method')) {
      final JsonRpcRequest req = JsonRpcRequest.fromJson(requestOrResponse);
      _observer?.processRequest(req);
      return true;
    }

    // response
    final JsonRpcResponse resp = JsonRpcResponse.fromJson(requestOrResponse);
    if (!resp.isValid()) {
      return true;
    }

    final JsonRpcRequest? req = _requests.remove(resp.id);
    if (req == null || !req.isValid()) {
      return true;
    }

    if (req.method == CLIENT_LOGIN) {
      if (resp.isMessage()) {
        _observer?.onConnectionStateChanged(ClientConnectionState.ACTIVE);
      }
    }

    _observer?.processResponse(req, resp);
    return true;
  }

  void _socketDataReceived(List<int> data) {
    _recvBuffer += data;
    _handleData();
  }

  void _handleData() {
    if (_recvBuffer.length < 4) {
      return;
    }

    final Uint8List dataSize = Uint8List.fromList(_recvBuffer.sublist(0, 4));
    final ByteData byteData = ByteData.view(dataSize.buffer);
    final int size = byteData.getUint32(0);

    final int messageSize = size + 4;
    if (_recvBuffer.length < messageSize) {
      return;
    }

    final payload = _recvBuffer.sublist(4, size);
    if (_processMessage(payload)) {
      _recvBuffer = _recvBuffer.sublist(messageSize);
      _handleData();
    }
  }

  // prepare methods
  List<int> _prepareToSend(String req) {
    if (_compressed == CompressedType.C_STANDART) {
      return gzip.encode(req.codeUnits);
    }
    return req.codeUnits;
  }

  List<int> _prepareToReceive(List<int> payload) {
    if (_compressed == CompressedType.C_STANDART) {
      return gzip.decode(payload);
    }
    return payload;
  }
}
